package com.apiFootball.Base;
import io.restassured.*;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.*;

public class RequestBuilder {
    protected RequestSpecification requestSpec;

    public RequestBuilder() {
        BaseMSApiFootball msApiFootball = new BaseMSApiFootball();
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(msApiFootball.getMicroservicio());
        builder.addHeader("x-rapidapi-key", msApiFootball.getAppApiKey());
        builder.addHeader("x-rapidapi-host", msApiFootball.getAppApiHost());
        builder.setContentType(ContentType.JSON);
        this.requestSpec = builder.build();
    }
}

