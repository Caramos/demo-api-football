package com.apiFootball.Base;

public class BaseMSApiFootball {
    private final String microservicio = "https://v3.football.api-sports.io";
    private final String appApiKey = "";
    private final String appApiHost = "v3.football.api-sports.io";


    public String getMicroservicio() {
        return this.microservicio;
    }

    public String getAppApiKey() {
        return appApiKey;
    }

    public String getAppApiHost() {
        return appApiHost;
    }
}
