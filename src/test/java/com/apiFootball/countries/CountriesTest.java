package com.apiFootball.countries;
import com.apiFootball.Base.*;
import static io.restassured.RestAssured.*;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

import com.apiFootball.valuesExpected.ValuesExpectedCountries;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;


public class CountriesTest extends RequestBuilder {

    private ValuesExpectedCountries valoresEsperados;
    private final String endopointCountries = "/countries";
    private final String pathSchemaJsonGetCountries = "src/test/resources/schemasJSON/countriesGetSchema.json";

    public CountriesTest() {
        super();
        enableLoggingOfRequestAndResponseIfValidationFails();
        valoresEsperados = new ValuesExpectedCountries();
    }

    @Test
    @DisplayName("La cantidad de paises recuperados es la correcta")
    public void consultarCantidadPaisesRecuperados() {
        given()
                .spec(this.requestSpec)
                .when()
                //.log().ifValidationFails()
                .get(this.endopointCountries)
                .then()
                .body("results",equalTo(valoresEsperados.getCantidadPaisesTotales()));
                //.log().ifValidationFails();
    }

    @Test
    @DisplayName("La cantidad de paginas devueltas es la correcta")
    public void consultaCantidadPaginasRecuperadas() {
        int paginaActual = 1;

        given()
                .spec(this.requestSpec)
                .when()
                .get(this.endopointCountries)
                .then()
                .body("paging.total",equalTo(valoresEsperados.getCantidadPaginasTotales()))
                .body("paging.current",equalTo(paginaActual));
    }
    @Test
    @DisplayName("El Schema del response Get /countries es el esperado")
    public void checkSchemaResponseGetCountries() {

        /*
         File directory = new File("./");
         System.out.println(directory.getAbsolutePath());
        */

        given()
                .spec(this.requestSpec)
                .when()
                .get(this.endopointCountries)
                .then()
                .body(JsonSchemaValidator.matchesJsonSchema(new File(this.pathSchemaJsonGetCountries)));
    }

    @Test
    @DisplayName("Get /Countries devuelve los países esperados")
    public void consultarPaisesDevueltosGetCountries() {

        given()
                .spec(this.requestSpec)
                .when()
                .get(this.endopointCountries)
                .then()
                .body("response.name",hasItems(valoresEsperados.getCountriesEsperados()));
    }

}
