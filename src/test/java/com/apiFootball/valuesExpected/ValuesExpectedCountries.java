package com.apiFootball.valuesExpected;

public class ValuesExpectedCountries {
    private int getCantidadPaisesTotales;
    private int getCantidadPaginasTotales;

    public int getCantidadPaisesTotales() {
        this.getCantidadPaisesTotales = 163;
        return getCantidadPaisesTotales;
    }

    public int getCantidadPaginasTotales() {
        this.getCantidadPaginasTotales = 1;
        return getCantidadPaginasTotales;
    }

    public String[]  getCountriesEsperados() {
        String[] countries = {
                "Albania",
                "Algeria",
                "Pakistan",
                "Paraguay",
                "Trinidad-And-Tobago",
                "World",
                "Wales",
                "Zambia"
        };
        return countries;
    }
}
